﻿using AcClient;
using Decal.Adapter;

using System;
using System.Runtime.InteropServices;

namespace EnableWindowsKey
{
    [FriendlyName("EnableWindowsKey")]
    public unsafe class PluginCore : PluginBase
    {
        protected override void Shutdown() { }

        protected override void Startup()
        {
            detour.Setup(new SetCursorDelegate(MyGenerateKeyboardEvent));
        }
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        internal delegate void SetCursorDelegate(CInputManager_WIN32* _this, tagMSG* msg);

        internal static Hook detour = new Hook((int)Entrypoint.CInputManager_WIN32__GenerateKeyboardEvent, (int)Call.CInputManager_WIN32__GenerateKeyboardEvent_0x0068AC95);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private const uint LP_WINDOWN = 0x015B0001;
        private const uint LP_WINUP = 0xC15B0001;

        private const uint WP_WIN = 0x5B;

        internal static void MyGenerateKeyboardEvent(CInputManager_WIN32* _this, tagMSG* i_msg)
        {
            //CoreManager.Current?.Actions?.AddChatText($"{i_msg->wParam:X} {i_msg->lParam:X}", 1);

            if (i_msg->wParam == WP_WIN && i_msg->lParam == LP_WINDOWN)
            {
                bool k = ShowWindow((IntPtr)i_msg->hwnd, 11);//minimize ac allowing windows key commands to work
            }
            _this->GenerateKeyboardEvent(i_msg);
            return;
        }
    }
}
