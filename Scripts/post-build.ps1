﻿param([string]$NuGetVersion,
     [string]$SolutionDir,
     [string]$ProjectDir,
     [string]$ProjectPath,
     [string]$ConfigurationName,
     [string]$TargetName,
     [string]$TargetDir,
     [string]$ProjectName,
     [string]$PlatformName,
     [string]$NuGetPackageRoot,
     [string]$TargetPath);

get-location | Write-Host

Remove-Item -Path "*Installer*.exe"

$nsis_script = "${SolutionDir}scripts\installer.nsi";

Write-Host """C:\Program Files (x86)\NSIS\Bin\makensis"" ""${nsis_script}"""
&"C:\Program Files (x86)\NSIS\Bin\makensis" ""${nsis_script}""

Move-Item -Path "*Installer-*.*.*.exe" -Destination "WindowsCursors-Installer-${NuGetVersion}.exe"

if (Test-Path "WindowsCursors.Installer.exe") {
  Remove-Item "WindowsCursors.Installer.exe"
}

Copy-Item -Path "*Installer-*.*.*.exe" -Destination "WindowsCursors.Installer.exe"
