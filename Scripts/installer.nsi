; Thanks to Trevis/SunnujDecalPlugins for the installer template!
; Define your application name
!define APPNAME "EnableWindowsKey"
!define NFNAME "EnableWindowsKey"
!define SOFTWARECOMPANY "FartwhifDecalPlugins"
!define VERSION	"1.0.0"
!define APPGUID "{DC1ED100-FA61-4395-998F-C8C3F93F8C04}"

!define ASSEMBLY "EnableWindowsKey.dll"
!define CLASSNAME "EnableWindowsKey.PluginCore"

!define BUILDPATH "..\bin\Release\NET481"

; Main Install settings
; compressor goes first
SetCompressor LZMA

Name "${APPNAME} ${VERSION}"
InstallDir "C:\Games\Decal Plugins\${APPNAME}"
InstallDirRegKey HKLM "Software\${SOFTWARECOMPANY}\${APPNAME}" ""
;SetFont "Verdana" 8
;Icon "Installer\Res\Decal.ico"
OutFile ".\..\bin\${APPNAME}Installer-${VERSION}.exe"

; Use compression

; Modern interface settings
!include "MUI.nsh"

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Set languages (first is default language)
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_RESERVEFILE_LANGDLL

Section "" CoreSection
; Set Section properties
	SetOverwrite on

	; Set Section Files and Shortcuts
	SetOutPath "$INSTDIR\"

	File "${BUILDPATH}\${ASSEMBLY}"
	File "${BUILDPATH}\CommandLine.dll"
	File "${BUILDPATH}\Microsoft.Bcl.AsyncInterfaces.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Configuration.Abstractions.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Configuration.Binder.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Configuration.dll"
	File "${BUILDPATH}\Microsoft.Extensions.DependencyInjection.Abstractions.dll"
	File "${BUILDPATH}\Microsoft.Extensions.DependencyInjection.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Logging.Abstractions.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Logging.Configuration.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Logging.Console.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Logging.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Options.ConfigurationExtensions.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Options.dll"
	File "${BUILDPATH}\Microsoft.Extensions.Primitives.dll"
	File "${BUILDPATH}\Newtonsoft.Json.dll"
	File "${BUILDPATH}\protobuf-net.Core.dll"
	File "${BUILDPATH}\protobuf-net.dll"
	File "${BUILDPATH}\RoyT.TrueType.dll"
	File "${BUILDPATH}\System.Buffers.dll"
	File "${BUILDPATH}\System.Collections.Immutable.dll"
	File "${BUILDPATH}\System.Diagnostics.DiagnosticSource.dll"
	File "${BUILDPATH}\System.Drawing.Common.dll"
	File "${BUILDPATH}\System.Memory.dll"
	File "${BUILDPATH}\System.Numerics.Vectors.dll"
	File "${BUILDPATH}\System.Runtime.CompilerServices.Unsafe.dll"
	File "${BUILDPATH}\System.Text.Encodings.Web.dll"
	File "${BUILDPATH}\System.Text.Json.dll"
	File "${BUILDPATH}\System.Threading.Tasks.Extensions.dll"
	File "${BUILDPATH}\System.ValueTuple.dll"
	File "${BUILDPATH}\UtilityBelt.Common.dll"
	File "${BUILDPATH}\UtilityBelt.Networking.dll"
	File "${BUILDPATH}\UtilityBelt.Scripting.dll"
	File "${BUILDPATH}\UtilityBelt.Service.dll"
	File "${BUILDPATH}\WattleScript.Interpreter.dll"
	
SectionEnd

Section -FinishSection

	WriteRegStr HKLM "Software\${SOFTWARECOMPANY}\${APPNAME}" "" "$INSTDIR"
	WriteRegStr HKLM "Software\${SOFTWARECOMPANY}\${APPNAME}" "Version" "${VERSION}"

	;Register in decal
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "" "${APPNAME}"
	WriteRegDWORD HKLM "Software\Decal\Plugins\${APPGUID}" "Enabled" "1"
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "Object" "${CLASSNAME}"
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "Assembly" "${ASSEMBLY}"
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "Path" "$INSTDIR"
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "Surrogate" "{71A69713-6593-47EC-0002-0000000DECA1}"
	WriteRegStr HKLM "Software\Decal\Plugins\${APPGUID}" "Uninstaller" "${APPNAME}"

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayName" "${APPNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteUninstaller "$INSTDIR\uninstall.exe"
    
    ;MessageBox MB_OK "Done"
SectionEnd

; Modern install component descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${CoreSection} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;Uninstall section
Section Uninstall

	;Remove from registry...
	DeleteRegKey HKLM "Software\${SOFTWARECOMPANY}\${APPNAME}"
	DeleteRegKey HKLM "Software\Decal\Plugins\${APPGUID}"
	DeleteRegKey HKLM "Software\Decal\NetworkFilters\${APPGUID}"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"

	; Delete self
	Delete "$INSTDIR\uninstall.exe"

	;Clean up
	Delete "$INSTDIR\${ASSEMBLY}"
	Delete "$INSTDIR\CommandLine.dll"
	Delete "$INSTDIR\Microsoft.Bcl.AsyncInterfaces.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Configuration.Abstractions.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Configuration.Binder.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Configuration.dll"
	Delete "$INSTDIR\Microsoft.Extensions.DependencyInjection.Abstractions.dll"
	Delete "$INSTDIR\Microsoft.Extensions.DependencyInjection.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Logging.Abstractions.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Logging.Configuration.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Logging.Console.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Logging.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Options.ConfigurationExtensions.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Options.dll"
	Delete "$INSTDIR\Microsoft.Extensions.Primitives.dll"
	Delete "$INSTDIR\Newtonsoft.Json.dll"
	Delete "$INSTDIR\protobuf-net.Core.dll"
	Delete "$INSTDIR\protobuf-net.dll"
	Delete "$INSTDIR\RoyT.TrueType.dll"
	Delete "$INSTDIR\System.Buffers.dll"
	Delete "$INSTDIR\System.Collections.Immutable.dll"
	Delete "$INSTDIR\System.Diagnostics.DiagnosticSource.dll"
	Delete "$INSTDIR\System.Drawing.Common.dll"
	Delete "$INSTDIR\System.Memory.dll"
	Delete "$INSTDIR\System.Numerics.Vectors.dll"
	Delete "$INSTDIR\System.Runtime.CompilerServices.Unsafe.dll"
	Delete "$INSTDIR\System.Text.Encodings.Web.dll"
	Delete "$INSTDIR\System.Text.Json.dll"
	Delete "$INSTDIR\System.Threading.Tasks.Extensions.dll"
	Delete "$INSTDIR\System.ValueTuple.dll"
	Delete "$INSTDIR\UtilityBelt.Common.dll"
	Delete "$INSTDIR\UtilityBelt.Networking.dll"
	Delete "$INSTDIR\UtilityBelt.Scripting.dll"
	Delete "$INSTDIR\UtilityBelt.Service.dll"
	Delete "$INSTDIR\WattleScript.Interpreter.dll"

	RMDir "$INSTDIR\"

SectionEnd

; eof
