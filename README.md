# EnableWindowsKey

Allow the Windows key to work (almost) normally while in game.  The game is minimized whenever the windows key is struck.  Normal Windows key based commands work, such as `Win+7` to switch to the seventh open app in the taskbar, or just `Win` to open the start menu and show the taskbar.  Compatible with Virindi Hotkey System - however it's probably best not to bind to windows key while using this plugin.

## Requirements

- Decal 2.9.8.3 or better
- .NET Framework 4.8.1 or better

## Getting started

Install the plugin and enable it via the Decal Agent.

## Known Issues

Only works with the left key.  Please let someone know if you need right.